package androidx.databinding.library.baseAdapters;

public class BR {
  public static final int _all = 0;

  public static final int oneViewModel = 1;

  public static final int twoViewModel = 2;
}
