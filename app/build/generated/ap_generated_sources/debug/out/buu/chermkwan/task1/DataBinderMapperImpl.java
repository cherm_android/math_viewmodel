package buu.chermkwan.task1;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import buu.chermkwan.task1.databinding.ActivityMainBindingImpl;
import buu.chermkwan.task1.databinding.FragmentOneBindingImpl;
import buu.chermkwan.task1.databinding.FragmentTwoBindingImpl;
import buu.chermkwan.task1.databinding.FragmentZeroBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYMAIN = 1;

  private static final int LAYOUT_FRAGMENTONE = 2;

  private static final int LAYOUT_FRAGMENTTWO = 3;

  private static final int LAYOUT_FRAGMENTZERO = 4;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(4);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(buu.chermkwan.task1.R.layout.activity_main, LAYOUT_ACTIVITYMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(buu.chermkwan.task1.R.layout.fragment_one, LAYOUT_FRAGMENTONE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(buu.chermkwan.task1.R.layout.fragment_two, LAYOUT_FRAGMENTTWO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(buu.chermkwan.task1.R.layout.fragment_zero, LAYOUT_FRAGMENTZERO);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYMAIN: {
          if ("layout/activity_main_0".equals(tag)) {
            return new ActivityMainBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_main is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTONE: {
          if ("layout/fragment_one_0".equals(tag)) {
            return new FragmentOneBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_one is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTTWO: {
          if ("layout/fragment_two_0".equals(tag)) {
            return new FragmentTwoBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_two is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTZERO: {
          if ("layout/fragment_zero_0".equals(tag)) {
            return new FragmentZeroBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_zero is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(3);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "oneViewModel");
      sKeys.put(2, "twoViewModel");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(4);

    static {
      sKeys.put("layout/activity_main_0", buu.chermkwan.task1.R.layout.activity_main);
      sKeys.put("layout/fragment_one_0", buu.chermkwan.task1.R.layout.fragment_one);
      sKeys.put("layout/fragment_two_0", buu.chermkwan.task1.R.layout.fragment_two);
      sKeys.put("layout/fragment_zero_0", buu.chermkwan.task1.R.layout.fragment_zero);
    }
  }
}
