package buu.chermkwan.task1.databinding;
import buu.chermkwan.task1.R;
import buu.chermkwan.task1.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentOneBindingImpl extends FragmentOneBinding implements buu.chermkwan.task1.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.equalText, 12);
        sViewsWithIds.put(R.id.guideLine, 13);
        sViewsWithIds.put(R.id.secondsremainingText, 14);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback2;
    @Nullable
    private final android.view.View.OnClickListener mCallback4;
    @Nullable
    private final android.view.View.OnClickListener mCallback3;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentOneBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 15, sIncludes, sViewsWithIds));
    }
    private FragmentOneBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 11
            , (android.widget.TextView) bindings[5]
            , (android.widget.Button) bindings[9]
            , (android.widget.Button) bindings[10]
            , (android.widget.Button) bindings[11]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[12]
            , (androidx.constraintlayout.widget.Guideline) bindings[13]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[4]
            , (android.widget.ProgressBar) bindings[1]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[8]
            );
        this.alertText.setTag(null);
        this.btnAns1.setTag(null);
        this.btnAns2.setTag(null);
        this.btnAns3.setTag(null);
        this.correctScoreText.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.number1Text.setTag(null);
        this.number2Text.setTag(null);
        this.operatorText.setTag(null);
        this.progressBar.setTag(null);
        this.timerText.setTag(null);
        this.wrongScoreText.setTag(null);
        setRootTag(root);
        // listeners
        mCallback2 = new buu.chermkwan.task1.generated.callback.OnClickListener(this, 1);
        mCallback4 = new buu.chermkwan.task1.generated.callback.OnClickListener(this, 3);
        mCallback3 = new buu.chermkwan.task1.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1000L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.oneViewModel == variableId) {
            setOneViewModel((buu.chermkwan.task1.game.OneViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setOneViewModel(@Nullable buu.chermkwan.task1.game.OneViewModel OneViewModel) {
        this.mOneViewModel = OneViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x800L;
        }
        notifyPropertyChanged(BR.oneViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeOneViewModelCurrentTime((androidx.lifecycle.LiveData<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeOneViewModelOperatorText((androidx.lifecycle.LiveData<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeOneViewModelAlertText((androidx.lifecycle.LiveData<java.lang.String>) object, fieldId);
            case 3 :
                return onChangeOneViewModelBtnAns1Text((androidx.lifecycle.LiveData<java.lang.String>) object, fieldId);
            case 4 :
                return onChangeOneViewModelWrongScoreText((androidx.lifecycle.LiveData<java.lang.String>) object, fieldId);
            case 5 :
                return onChangeOneViewModelBtnAns3Text((androidx.lifecycle.LiveData<java.lang.String>) object, fieldId);
            case 6 :
                return onChangeOneViewModelRandomNum1((androidx.lifecycle.LiveData<java.lang.Integer>) object, fieldId);
            case 7 :
                return onChangeOneViewModelProgressTime((androidx.lifecycle.LiveData<java.lang.Integer>) object, fieldId);
            case 8 :
                return onChangeOneViewModelCorrectScoreText((androidx.lifecycle.LiveData<java.lang.String>) object, fieldId);
            case 9 :
                return onChangeOneViewModelBtnAns2Text((androidx.lifecycle.LiveData<java.lang.String>) object, fieldId);
            case 10 :
                return onChangeOneViewModelRandomNum2((androidx.lifecycle.LiveData<java.lang.Integer>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeOneViewModelCurrentTime(androidx.lifecycle.LiveData<java.lang.String> OneViewModelCurrentTime, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeOneViewModelOperatorText(androidx.lifecycle.LiveData<java.lang.String> OneViewModelOperatorText, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeOneViewModelAlertText(androidx.lifecycle.LiveData<java.lang.String> OneViewModelAlertText, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeOneViewModelBtnAns1Text(androidx.lifecycle.LiveData<java.lang.String> OneViewModelBtnAns1Text, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeOneViewModelWrongScoreText(androidx.lifecycle.LiveData<java.lang.String> OneViewModelWrongScoreText, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeOneViewModelBtnAns3Text(androidx.lifecycle.LiveData<java.lang.String> OneViewModelBtnAns3Text, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeOneViewModelRandomNum1(androidx.lifecycle.LiveData<java.lang.Integer> OneViewModelRandomNum1, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeOneViewModelProgressTime(androidx.lifecycle.LiveData<java.lang.Integer> OneViewModelProgressTime, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x80L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeOneViewModelCorrectScoreText(androidx.lifecycle.LiveData<java.lang.String> OneViewModelCorrectScoreText, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x100L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeOneViewModelBtnAns2Text(androidx.lifecycle.LiveData<java.lang.String> OneViewModelBtnAns2Text, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x200L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeOneViewModelRandomNum2(androidx.lifecycle.LiveData<java.lang.Integer> OneViewModelRandomNum2, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x400L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.lifecycle.LiveData<java.lang.String> oneViewModelCurrentTime = null;
        androidx.lifecycle.LiveData<java.lang.String> oneViewModelOperatorText = null;
        androidx.lifecycle.LiveData<java.lang.String> oneViewModelAlertText = null;
        java.lang.String oneViewModelBtnAns2TextGetValue = null;
        androidx.lifecycle.LiveData<java.lang.String> oneViewModelBtnAns1Text = null;
        java.lang.String stringValueOfOneViewModelBtnAns3Text = null;
        java.lang.String oneViewModelWrongScoreTextGetValue = null;
        androidx.lifecycle.LiveData<java.lang.String> oneViewModelWrongScoreText = null;
        java.lang.String oneViewModelCorrectScoreTextGetValue = null;
        java.lang.String oneViewModelBtnAns3TextGetValue = null;
        androidx.lifecycle.LiveData<java.lang.String> oneViewModelBtnAns3Text = null;
        java.lang.String oneViewModelAlertTextGetValue = null;
        androidx.lifecycle.LiveData<java.lang.Integer> oneViewModelRandomNum1 = null;
        int androidxDatabindingViewDataBindingSafeUnboxOneViewModelRandomNum1GetValue = 0;
        int androidxDatabindingViewDataBindingSafeUnboxOneViewModelRandomNum2GetValue = 0;
        java.lang.Integer oneViewModelProgressTimeGetValue = null;
        java.lang.String stringValueOfOneViewModelRandomNum2 = null;
        java.lang.Integer oneViewModelRandomNum1GetValue = null;
        androidx.lifecycle.LiveData<java.lang.Integer> oneViewModelProgressTime = null;
        java.lang.String stringValueOfOneViewModelCurrentTime = null;
        java.lang.String stringValueOfOneViewModelBtnAns2Text = null;
        androidx.lifecycle.LiveData<java.lang.String> oneViewModelCorrectScoreText = null;
        androidx.lifecycle.LiveData<java.lang.String> oneViewModelBtnAns2Text = null;
        java.lang.String oneViewModelCurrentTimeGetValue = null;
        int androidxDatabindingViewDataBindingSafeUnboxOneViewModelProgressTimeGetValue = 0;
        androidx.lifecycle.LiveData<java.lang.Integer> oneViewModelRandomNum2 = null;
        java.lang.String oneViewModelBtnAns1TextGetValue = null;
        java.lang.String oneViewModelOperatorTextGetValue = null;
        java.lang.String stringValueOfOneViewModelBtnAns1Text = null;
        java.lang.Integer oneViewModelRandomNum2GetValue = null;
        java.lang.String stringValueOfOneViewModelRandomNum1 = null;
        buu.chermkwan.task1.game.OneViewModel oneViewModel = mOneViewModel;

        if ((dirtyFlags & 0x1fffL) != 0) {


            if ((dirtyFlags & 0x1801L) != 0) {

                    if (oneViewModel != null) {
                        // read oneViewModel.currentTime
                        oneViewModelCurrentTime = oneViewModel.getCurrentTime();
                    }
                    updateLiveDataRegistration(0, oneViewModelCurrentTime);


                    if (oneViewModelCurrentTime != null) {
                        // read oneViewModel.currentTime.getValue()
                        oneViewModelCurrentTimeGetValue = oneViewModelCurrentTime.getValue();
                    }


                    // read String.valueOf(oneViewModel.currentTime.getValue())
                    stringValueOfOneViewModelCurrentTime = java.lang.String.valueOf(oneViewModelCurrentTimeGetValue);
            }
            if ((dirtyFlags & 0x1802L) != 0) {

                    if (oneViewModel != null) {
                        // read oneViewModel.operatorText
                        oneViewModelOperatorText = oneViewModel.getOperatorText();
                    }
                    updateLiveDataRegistration(1, oneViewModelOperatorText);


                    if (oneViewModelOperatorText != null) {
                        // read oneViewModel.operatorText.getValue()
                        oneViewModelOperatorTextGetValue = oneViewModelOperatorText.getValue();
                    }
            }
            if ((dirtyFlags & 0x1804L) != 0) {

                    if (oneViewModel != null) {
                        // read oneViewModel.alertText
                        oneViewModelAlertText = oneViewModel.getAlertText();
                    }
                    updateLiveDataRegistration(2, oneViewModelAlertText);


                    if (oneViewModelAlertText != null) {
                        // read oneViewModel.alertText.getValue()
                        oneViewModelAlertTextGetValue = oneViewModelAlertText.getValue();
                    }
            }
            if ((dirtyFlags & 0x1808L) != 0) {

                    if (oneViewModel != null) {
                        // read oneViewModel.btnAns1Text
                        oneViewModelBtnAns1Text = oneViewModel.getBtnAns1Text();
                    }
                    updateLiveDataRegistration(3, oneViewModelBtnAns1Text);


                    if (oneViewModelBtnAns1Text != null) {
                        // read oneViewModel.btnAns1Text.getValue()
                        oneViewModelBtnAns1TextGetValue = oneViewModelBtnAns1Text.getValue();
                    }


                    // read String.valueOf(oneViewModel.btnAns1Text.getValue())
                    stringValueOfOneViewModelBtnAns1Text = java.lang.String.valueOf(oneViewModelBtnAns1TextGetValue);
            }
            if ((dirtyFlags & 0x1810L) != 0) {

                    if (oneViewModel != null) {
                        // read oneViewModel.wrongScoreText
                        oneViewModelWrongScoreText = oneViewModel.getWrongScoreText();
                    }
                    updateLiveDataRegistration(4, oneViewModelWrongScoreText);


                    if (oneViewModelWrongScoreText != null) {
                        // read oneViewModel.wrongScoreText.getValue()
                        oneViewModelWrongScoreTextGetValue = oneViewModelWrongScoreText.getValue();
                    }
            }
            if ((dirtyFlags & 0x1820L) != 0) {

                    if (oneViewModel != null) {
                        // read oneViewModel.btnAns3Text
                        oneViewModelBtnAns3Text = oneViewModel.getBtnAns3Text();
                    }
                    updateLiveDataRegistration(5, oneViewModelBtnAns3Text);


                    if (oneViewModelBtnAns3Text != null) {
                        // read oneViewModel.btnAns3Text.getValue()
                        oneViewModelBtnAns3TextGetValue = oneViewModelBtnAns3Text.getValue();
                    }


                    // read String.valueOf(oneViewModel.btnAns3Text.getValue())
                    stringValueOfOneViewModelBtnAns3Text = java.lang.String.valueOf(oneViewModelBtnAns3TextGetValue);
            }
            if ((dirtyFlags & 0x1840L) != 0) {

                    if (oneViewModel != null) {
                        // read oneViewModel.randomNum1
                        oneViewModelRandomNum1 = oneViewModel.getRandomNum1();
                    }
                    updateLiveDataRegistration(6, oneViewModelRandomNum1);


                    if (oneViewModelRandomNum1 != null) {
                        // read oneViewModel.randomNum1.getValue()
                        oneViewModelRandomNum1GetValue = oneViewModelRandomNum1.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(oneViewModel.randomNum1.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxOneViewModelRandomNum1GetValue = androidx.databinding.ViewDataBinding.safeUnbox(oneViewModelRandomNum1GetValue);


                    // read String.valueOf(androidx.databinding.ViewDataBinding.safeUnbox(oneViewModel.randomNum1.getValue()))
                    stringValueOfOneViewModelRandomNum1 = java.lang.String.valueOf(androidxDatabindingViewDataBindingSafeUnboxOneViewModelRandomNum1GetValue);
            }
            if ((dirtyFlags & 0x1880L) != 0) {

                    if (oneViewModel != null) {
                        // read oneViewModel.progressTime
                        oneViewModelProgressTime = oneViewModel.getProgressTime();
                    }
                    updateLiveDataRegistration(7, oneViewModelProgressTime);


                    if (oneViewModelProgressTime != null) {
                        // read oneViewModel.progressTime.getValue()
                        oneViewModelProgressTimeGetValue = oneViewModelProgressTime.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(oneViewModel.progressTime.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxOneViewModelProgressTimeGetValue = androidx.databinding.ViewDataBinding.safeUnbox(oneViewModelProgressTimeGetValue);
            }
            if ((dirtyFlags & 0x1900L) != 0) {

                    if (oneViewModel != null) {
                        // read oneViewModel.correctScoreText
                        oneViewModelCorrectScoreText = oneViewModel.getCorrectScoreText();
                    }
                    updateLiveDataRegistration(8, oneViewModelCorrectScoreText);


                    if (oneViewModelCorrectScoreText != null) {
                        // read oneViewModel.correctScoreText.getValue()
                        oneViewModelCorrectScoreTextGetValue = oneViewModelCorrectScoreText.getValue();
                    }
            }
            if ((dirtyFlags & 0x1a00L) != 0) {

                    if (oneViewModel != null) {
                        // read oneViewModel.btnAns2Text
                        oneViewModelBtnAns2Text = oneViewModel.getBtnAns2Text();
                    }
                    updateLiveDataRegistration(9, oneViewModelBtnAns2Text);


                    if (oneViewModelBtnAns2Text != null) {
                        // read oneViewModel.btnAns2Text.getValue()
                        oneViewModelBtnAns2TextGetValue = oneViewModelBtnAns2Text.getValue();
                    }


                    // read String.valueOf(oneViewModel.btnAns2Text.getValue())
                    stringValueOfOneViewModelBtnAns2Text = java.lang.String.valueOf(oneViewModelBtnAns2TextGetValue);
            }
            if ((dirtyFlags & 0x1c00L) != 0) {

                    if (oneViewModel != null) {
                        // read oneViewModel.randomNum2
                        oneViewModelRandomNum2 = oneViewModel.getRandomNum2();
                    }
                    updateLiveDataRegistration(10, oneViewModelRandomNum2);


                    if (oneViewModelRandomNum2 != null) {
                        // read oneViewModel.randomNum2.getValue()
                        oneViewModelRandomNum2GetValue = oneViewModelRandomNum2.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(oneViewModel.randomNum2.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxOneViewModelRandomNum2GetValue = androidx.databinding.ViewDataBinding.safeUnbox(oneViewModelRandomNum2GetValue);


                    // read String.valueOf(androidx.databinding.ViewDataBinding.safeUnbox(oneViewModel.randomNum2.getValue()))
                    stringValueOfOneViewModelRandomNum2 = java.lang.String.valueOf(androidxDatabindingViewDataBindingSafeUnboxOneViewModelRandomNum2GetValue);
            }
        }
        // batch finished
        if ((dirtyFlags & 0x1804L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.alertText, oneViewModelAlertTextGetValue);
        }
        if ((dirtyFlags & 0x1808L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.btnAns1, stringValueOfOneViewModelBtnAns1Text);
        }
        if ((dirtyFlags & 0x1000L) != 0) {
            // api target 1

            this.btnAns1.setOnClickListener(mCallback2);
            this.btnAns2.setOnClickListener(mCallback3);
            this.btnAns3.setOnClickListener(mCallback4);
        }
        if ((dirtyFlags & 0x1a00L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.btnAns2, stringValueOfOneViewModelBtnAns2Text);
        }
        if ((dirtyFlags & 0x1820L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.btnAns3, stringValueOfOneViewModelBtnAns3Text);
        }
        if ((dirtyFlags & 0x1900L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.correctScoreText, oneViewModelCorrectScoreTextGetValue);
        }
        if ((dirtyFlags & 0x1840L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.number1Text, stringValueOfOneViewModelRandomNum1);
        }
        if ((dirtyFlags & 0x1c00L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.number2Text, stringValueOfOneViewModelRandomNum2);
        }
        if ((dirtyFlags & 0x1802L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.operatorText, oneViewModelOperatorTextGetValue);
        }
        if ((dirtyFlags & 0x1880L) != 0) {
            // api target 1

            this.progressBar.setProgress(androidxDatabindingViewDataBindingSafeUnboxOneViewModelProgressTimeGetValue);
        }
        if ((dirtyFlags & 0x1801L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.timerText, stringValueOfOneViewModelCurrentTime);
        }
        if ((dirtyFlags & 0x1810L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.wrongScoreText, oneViewModelWrongScoreTextGetValue);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 1: {
                // localize variables for thread safety
                // oneViewModel != null
                boolean oneViewModelJavaLangObjectNull = false;
                // oneViewModel
                buu.chermkwan.task1.game.OneViewModel oneViewModel = mOneViewModel;



                oneViewModelJavaLangObjectNull = (oneViewModel) != (null);
                if (oneViewModelJavaLangObjectNull) {


                    oneViewModel.choosebtnAns1();
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // oneViewModel != null
                boolean oneViewModelJavaLangObjectNull = false;
                // oneViewModel
                buu.chermkwan.task1.game.OneViewModel oneViewModel = mOneViewModel;



                oneViewModelJavaLangObjectNull = (oneViewModel) != (null);
                if (oneViewModelJavaLangObjectNull) {


                    oneViewModel.choosebtnAns3();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // oneViewModel != null
                boolean oneViewModelJavaLangObjectNull = false;
                // oneViewModel
                buu.chermkwan.task1.game.OneViewModel oneViewModel = mOneViewModel;



                oneViewModelJavaLangObjectNull = (oneViewModel) != (null);
                if (oneViewModelJavaLangObjectNull) {


                    oneViewModel.choosebtnAns2();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): oneViewModel.currentTime
        flag 1 (0x2L): oneViewModel.operatorText
        flag 2 (0x3L): oneViewModel.alertText
        flag 3 (0x4L): oneViewModel.btnAns1Text
        flag 4 (0x5L): oneViewModel.wrongScoreText
        flag 5 (0x6L): oneViewModel.btnAns3Text
        flag 6 (0x7L): oneViewModel.randomNum1
        flag 7 (0x8L): oneViewModel.progressTime
        flag 8 (0x9L): oneViewModel.correctScoreText
        flag 9 (0xaL): oneViewModel.btnAns2Text
        flag 10 (0xbL): oneViewModel.randomNum2
        flag 11 (0xcL): oneViewModel
        flag 12 (0xdL): null
    flag mapping end*/
    //end
}