package buu.chermkwan.task1.databinding;
import buu.chermkwan.task1.R;
import buu.chermkwan.task1.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentTwoBindingImpl extends FragmentTwoBinding implements buu.chermkwan.task1.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.youScoredText, 4);
        sViewsWithIds.put(R.id.slashText, 5);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback1;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentTwoBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds));
    }
    private FragmentTwoBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (android.widget.Button) bindings[1]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[4]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.playAgainButton.setTag(null);
        this.scoreText.setTag(null);
        this.sumScoreText.setTag(null);
        setRootTag(root);
        // listeners
        mCallback1 = new buu.chermkwan.task1.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.twoViewModel == variableId) {
            setTwoViewModel((buu.chermkwan.task1.score.TwoViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setTwoViewModel(@Nullable buu.chermkwan.task1.score.TwoViewModel TwoViewModel) {
        this.mTwoViewModel = TwoViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.twoViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeTwoViewModelCorrectScore((androidx.lifecycle.LiveData<java.lang.Integer>) object, fieldId);
            case 1 :
                return onChangeTwoViewModelSumScore((androidx.lifecycle.LiveData<java.lang.Integer>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeTwoViewModelCorrectScore(androidx.lifecycle.LiveData<java.lang.Integer> TwoViewModelCorrectScore, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeTwoViewModelSumScore(androidx.lifecycle.LiveData<java.lang.Integer> TwoViewModelSumScore, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int androidxDatabindingViewDataBindingSafeUnboxTwoViewModelSumScoreGetValue = 0;
        androidx.lifecycle.LiveData<java.lang.Integer> twoViewModelCorrectScore = null;
        java.lang.Integer twoViewModelCorrectScoreGetValue = null;
        java.lang.Integer twoViewModelSumScoreGetValue = null;
        buu.chermkwan.task1.score.TwoViewModel twoViewModel = mTwoViewModel;
        java.lang.String stringValueOfTwoViewModelSumScore = null;
        java.lang.String stringValueOfTwoViewModelCorrectScore = null;
        androidx.lifecycle.LiveData<java.lang.Integer> twoViewModelSumScore = null;
        int androidxDatabindingViewDataBindingSafeUnboxTwoViewModelCorrectScoreGetValue = 0;

        if ((dirtyFlags & 0xfL) != 0) {


            if ((dirtyFlags & 0xdL) != 0) {

                    if (twoViewModel != null) {
                        // read twoViewModel.correctScore
                        twoViewModelCorrectScore = twoViewModel.getCorrectScore();
                    }
                    updateLiveDataRegistration(0, twoViewModelCorrectScore);


                    if (twoViewModelCorrectScore != null) {
                        // read twoViewModel.correctScore.getValue()
                        twoViewModelCorrectScoreGetValue = twoViewModelCorrectScore.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(twoViewModel.correctScore.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxTwoViewModelCorrectScoreGetValue = androidx.databinding.ViewDataBinding.safeUnbox(twoViewModelCorrectScoreGetValue);


                    // read String.valueOf(androidx.databinding.ViewDataBinding.safeUnbox(twoViewModel.correctScore.getValue()))
                    stringValueOfTwoViewModelCorrectScore = java.lang.String.valueOf(androidxDatabindingViewDataBindingSafeUnboxTwoViewModelCorrectScoreGetValue);
            }
            if ((dirtyFlags & 0xeL) != 0) {

                    if (twoViewModel != null) {
                        // read twoViewModel.sumScore
                        twoViewModelSumScore = twoViewModel.getSumScore();
                    }
                    updateLiveDataRegistration(1, twoViewModelSumScore);


                    if (twoViewModelSumScore != null) {
                        // read twoViewModel.sumScore.getValue()
                        twoViewModelSumScoreGetValue = twoViewModelSumScore.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(twoViewModel.sumScore.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxTwoViewModelSumScoreGetValue = androidx.databinding.ViewDataBinding.safeUnbox(twoViewModelSumScoreGetValue);


                    // read String.valueOf(androidx.databinding.ViewDataBinding.safeUnbox(twoViewModel.sumScore.getValue()))
                    stringValueOfTwoViewModelSumScore = java.lang.String.valueOf(androidxDatabindingViewDataBindingSafeUnboxTwoViewModelSumScoreGetValue);
            }
        }
        // batch finished
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            this.playAgainButton.setOnClickListener(mCallback1);
        }
        if ((dirtyFlags & 0xdL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.scoreText, stringValueOfTwoViewModelCorrectScore);
        }
        if ((dirtyFlags & 0xeL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.sumScoreText, stringValueOfTwoViewModelSumScore);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // twoViewModel
        buu.chermkwan.task1.score.TwoViewModel twoViewModel = mTwoViewModel;
        // twoViewModel != null
        boolean twoViewModelJavaLangObjectNull = false;



        twoViewModelJavaLangObjectNull = (twoViewModel) != (null);
        if (twoViewModelJavaLangObjectNull) {


            twoViewModel.onPlayAgain();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): twoViewModel.correctScore
        flag 1 (0x2L): twoViewModel.sumScore
        flag 2 (0x3L): twoViewModel
        flag 3 (0x4L): null
    flag mapping end*/
    //end
}