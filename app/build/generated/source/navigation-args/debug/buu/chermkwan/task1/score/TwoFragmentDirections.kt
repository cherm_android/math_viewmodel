package buu.chermkwan.task1.score

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import buu.chermkwan.task1.R

class TwoFragmentDirections private constructor() {
  companion object {
    fun actionRestart(): NavDirections = ActionOnlyNavDirections(R.id.action_restart)
  }
}
