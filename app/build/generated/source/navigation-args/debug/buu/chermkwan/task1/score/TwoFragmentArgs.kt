package buu.chermkwan.task1.score

import android.os.Bundle
import androidx.navigation.NavArgs
import kotlin.Int
import kotlin.jvm.JvmStatic

data class TwoFragmentArgs(
  val correctScore: Int = 0,
  val sumScore: Int = 0
) : NavArgs {
  fun toBundle(): Bundle {
    val result = Bundle()
    result.putInt("correctScore", this.correctScore)
    result.putInt("sumScore", this.sumScore)
    return result
  }

  companion object {
    @JvmStatic
    fun fromBundle(bundle: Bundle): TwoFragmentArgs {
      bundle.setClassLoader(TwoFragmentArgs::class.java.classLoader)
      val __correctScore : Int
      if (bundle.containsKey("correctScore")) {
        __correctScore = bundle.getInt("correctScore")
      } else {
        __correctScore = 0
      }
      val __sumScore : Int
      if (bundle.containsKey("sumScore")) {
        __sumScore = bundle.getInt("sumScore")
      } else {
        __sumScore = 0
      }
      return TwoFragmentArgs(__correctScore, __sumScore)
    }
  }
}
