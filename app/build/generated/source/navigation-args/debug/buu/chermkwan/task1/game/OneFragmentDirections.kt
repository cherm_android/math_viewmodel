package buu.chermkwan.task1.game

import android.os.Bundle
import androidx.navigation.NavDirections
import buu.chermkwan.task1.R
import kotlin.Int

class OneFragmentDirections private constructor() {
  private data class ActionOneFragmentToTwoFragment(
    val correctScore: Int = 0,
    val sumScore: Int = 0
  ) : NavDirections {
    override fun getActionId(): Int = R.id.action_oneFragment_to_twoFragment

    override fun getArguments(): Bundle {
      val result = Bundle()
      result.putInt("correctScore", this.correctScore)
      result.putInt("sumScore", this.sumScore)
      return result
    }
  }

  companion object {
    fun actionOneFragmentToTwoFragment(correctScore: Int = 0, sumScore: Int = 0): NavDirections =
        ActionOneFragmentToTwoFragment(correctScore, sumScore)
  }
}
