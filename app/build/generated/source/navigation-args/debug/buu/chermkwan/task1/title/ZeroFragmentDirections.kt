package buu.chermkwan.task1.title

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import buu.chermkwan.task1.R

class ZeroFragmentDirections private constructor() {
  companion object {
    fun actionZeroFragmentToOneFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_zeroFragment_to_oneFragment)
  }
}
