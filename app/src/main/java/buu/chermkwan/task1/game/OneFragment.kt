package buu.chermkwan.task1.game

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import buu.chermkwan.task1.R
import buu.chermkwan.task1.databinding.FragmentOneBinding

class OneFragment : Fragment() {

    private lateinit var viewModel: OneViewModel
    private lateinit var binding: FragmentOneBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<FragmentOneBinding>(inflater,
            R.layout.fragment_one, container, false)
        Log.i("GameFragment", "Called ViewModelProvider.get")

        viewModel = ViewModelProvider(this).get(OneViewModel::class.java)

        binding.oneViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        viewModel.eventGameFinish.observe(viewLifecycleOwner, Observer<Boolean> { hasFinished ->
            if (hasFinished) whenFinished()
        })

        return binding.root
    }

    private fun whenFinished() {
        val sumScore = (viewModel.correctScore.value)?.plus(viewModel.wrongScore.value!!)
        val action = OneFragmentDirections.actionOneFragmentToTwoFragment(viewModel.correctScore.value!!, sumScore!!)
        findNavController().navigate(action)
        viewModel.onGameFinishComplete()
    }

}