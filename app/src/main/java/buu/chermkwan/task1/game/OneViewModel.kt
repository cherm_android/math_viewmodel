package buu.chermkwan.task1.game

import android.os.CountDownTimer
import android.provider.Settings.Secure.getString
import android.text.format.DateUtils
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import buu.chermkwan.task1.R
import kotlinx.android.synthetic.main.fragment_one.*
import kotlin.random.Random

class OneViewModel : ViewModel () {

    private val timer: CountDownTimer

    companion object {
        private const val ONE_SECOND = 1000L
        private const val COUNTDOWN_TIME = 20000L
    }

    private val _correctScore = MutableLiveData<Int>()
    val correctScore:LiveData<Int>
        get() = _correctScore

    private val _wrongScore = MutableLiveData<Int>()
    val wrongScore:LiveData<Int>
        get() = _wrongScore

    private val _correctScoreText = MutableLiveData<String>()
    val correctScoreText: LiveData<String>
        get() = _correctScoreText

    private val _wrongScoreText = MutableLiveData<String>()
    val wrongScoreText: LiveData<String>
        get() = _wrongScoreText

    private val _alertText = MutableLiveData<String>()
    val alertText: LiveData<String>
        get() = _alertText

    private val _progressTime = MutableLiveData<Int>()
    val progressTime:LiveData<Int>
        get() = _progressTime

    private val _randomNum1 = MutableLiveData<Int>()
    val randomNum1:LiveData<Int>
        get() = _randomNum1

    private val _randomNum2 = MutableLiveData<Int>()
    val randomNum2:LiveData<Int>
        get() = _randomNum2

    private val _randomOperator = MutableLiveData<Int>()
    val randomOperator:LiveData<Int>
        get() = _randomOperator

    private val _operatorText = MutableLiveData<String>()
    val operatorText: LiveData<String>
        get() = _operatorText

    private val _randomBtn = MutableLiveData<Int>()
    val randomBtn:LiveData<Int>
        get() = _randomBtn

    private val _btnAns1Text = MutableLiveData<String>()
    val btnAns1Text: LiveData<String>
        get() = _btnAns1Text

    private val _btnAns2Text = MutableLiveData<String>()
    val btnAns2Text: LiveData<String>
        get() = _btnAns2Text

    private val _btnAns3Text = MutableLiveData<String>()
    val btnAns3Text: LiveData<String>
        get() = _btnAns3Text

    private val _Answer = MutableLiveData<Int>()
    val Answer:LiveData<Int>
        get() = _Answer

    private val _currentTime = MutableLiveData<String>()
    val currentTime: LiveData<String>
        get() = _currentTime

    private val _eventGameFinish = MutableLiveData<Boolean>()
    val eventGameFinish: LiveData<Boolean>
        get() = _eventGameFinish

    init {
        _correctScore.value = 0
        _wrongScore.value = 0
        _correctScoreText.value = "Correct 0"
        _wrongScoreText.value = "Wrong 0"
        _alertText.value = ""
        _progressTime.value = 100
        _randomNum1.value = 0
        _randomNum2.value = 0
        _randomOperator.value = 0
        _operatorText.value = ""
        _randomBtn.value = 0
        _Answer.value = 0
        Log.i("GameViewModel", "GameViewModel created!")
        timer = object : CountDownTimer(COUNTDOWN_TIME, ONE_SECOND) {
            override fun onTick(millisUntilFinished: Long) {
                _currentTime.value = (millisUntilFinished/ONE_SECOND).toString()
                countDown()
            }
            override fun onFinish() {
                onGameFinish()
            }
        }
        timer.start()
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("GameViewModel", "GameViewModel destroyed!")
    }

    fun countDown() {
        _progressTime.value = (_progressTime.value)?.minus(5)
    }

    private fun game() {
        randomNum()
    }

    fun randomNum() {
        _randomNum1.value = Random.nextInt(0, 10)
        _randomNum2.value = Random.nextInt(0, 10)
        randomCal()
    }

    fun randomCal() {
        _randomOperator.value = Random.nextInt(0, 3)
        if (_randomOperator.value == 0) {
            _operatorText.value = "+"
            plus()
            randombutton()
        } else if (_randomOperator.value == 1) {
            _operatorText.value = "-"
            minus()
            randombutton()
        } else {
            _operatorText.value = "X"
            multiply()
            randombutton()
        }
    }

    fun plus() {
        _Answer.value = (_randomNum1.value)?.plus(_randomNum2.value!!)
    }

    fun multiply() {
        _Answer.value = _randomNum1.value!! * _randomNum2.value!!
    }

    fun minus() {
        _Answer.value = (_randomNum1.value)?.minus(_randomNum2.value!!)
    }

    fun randombutton() {
        _randomBtn.value = Random.nextInt(0, 3)
        if (_randomBtn.value == 0) {
            _btnAns1Text.value = _Answer.value.toString()
            _btnAns2Text.value = (_Answer.value)?.minus(1).toString()
            _btnAns3Text.value = (_Answer.value)?.minus(2).toString()
        } else if (_randomBtn.value == 1) {
            _btnAns2Text.value = _Answer.value.toString()
            _btnAns1Text.value = (_Answer.value)?.plus(2).toString()
            _btnAns3Text.value = (_Answer.value)?.plus(1).toString()
        } else {
            _btnAns3Text.value = _Answer.value.toString()
            _btnAns1Text.value = (_Answer.value)?.minus(1).toString()
            _btnAns2Text.value = (_Answer.value)?.plus(1).toString()
        }
    }

    fun whenCorrect() {
        _correctScore.value = (_correctScore.value)?.plus(1)
        _alertText.value = "Correct"
        _correctScoreText.value = "Correct " + _correctScore.value.toString()
    }

    fun whenWrong() {
        _wrongScore.value = (_wrongScore.value)?.plus(1)
        _alertText.value = "Wrong"
        _wrongScoreText.value = "Wrong " + _wrongScore.value.toString()
    }

    fun choosebtnAns1() {
        if (_btnAns1Text.value == _Answer.value.toString()) {
            whenCorrect()
            game()
        } else {
            whenWrong()
            game()
        }
    }

    fun choosebtnAns2() {
        if (_btnAns2Text.value == _Answer.value.toString()) {
            whenCorrect()
            game()
        } else {
            whenWrong()
            game()
        }
    }

    fun choosebtnAns3() {
        if (_btnAns3Text.value == _Answer.value.toString()) {
            whenCorrect()
            game()
        } else {
            whenWrong()
            game()
        }
    }

    fun onGameFinish() {
        _eventGameFinish.value = true
    }

    fun onGameFinishComplete() {
        _eventGameFinish.value = false
    }

}