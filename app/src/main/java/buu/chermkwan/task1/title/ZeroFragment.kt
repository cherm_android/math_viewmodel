package buu.chermkwan.task1.title

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import buu.chermkwan.task1.R
import buu.chermkwan.task1.databinding.FragmentZeroBinding
import buu.chermkwan.task1.score.TwoFragmentDirections

class ZeroFragment : Fragment() {
    private lateinit var binding:FragmentZeroBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate<FragmentZeroBinding>(inflater,
            R.layout.fragment_zero, container, false)

        binding.playGameButton.setOnClickListener {
            findNavController().navigate(ZeroFragmentDirections.actionZeroFragmentToOneFragment())
        }

        return binding.root
    }

}