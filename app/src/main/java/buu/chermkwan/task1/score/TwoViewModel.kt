package buu.chermkwan.task1.score

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TwoViewModel(correctScore: Int, sumScore: Int) : ViewModel() {

    private val _correctScore = MutableLiveData<Int>()
    val correctScore: LiveData<Int>
        get() = _correctScore

    private val _sumScore = MutableLiveData<Int>()
    val sumScore: LiveData<Int>
        get() = _sumScore

    private val _eventPlayAgain = MutableLiveData<Boolean>()
    val eventPlayAgain: LiveData<Boolean>
        get() = _eventPlayAgain

    init {
        _correctScore.value = correctScore
        _sumScore.value = sumScore
        Log.i("ScoreViewModel", "Final correctScore is $correctScore and Final sumScore is $sumScore")
    }

    fun onPlayAgain() {
        _eventPlayAgain.value = true
    }
    fun onPlayAgainComplete() {
        _eventPlayAgain.value = false
    }

}