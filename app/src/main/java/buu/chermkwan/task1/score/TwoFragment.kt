package buu.chermkwan.task1.score

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import buu.chermkwan.task1.R
import buu.chermkwan.task1.databinding.FragmentTwoBinding
import buu.chermkwan.task1.score.TwoFragmentDirections as TwoFragmentDirections1

class TwoFragment : Fragment() {
    lateinit var binding: FragmentTwoBinding
    private lateinit var viewModel: TwoViewModel
    private lateinit var viewModelFactory: TwoViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentTwoBinding>(inflater,
            R.layout.fragment_two, container, false)

        viewModelFactory = TwoViewModelFactory(
            TwoFragmentArgs.fromBundle(requireArguments()).correctScore,
            TwoFragmentArgs.fromBundle(requireArguments()).sumScore
        )

        viewModel = ViewModelProvider(this, viewModelFactory).get(TwoViewModel::class.java)

        binding.twoViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        viewModel.eventPlayAgain.observe(viewLifecycleOwner, Observer { playAgain ->
            if (playAgain) {
                findNavController().navigate(TwoFragmentDirections1.actionRestart())
                viewModel.onPlayAgainComplete()
            }
        })

        return binding.root
    }

}