package buu.chermkwan.task1.score

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class TwoViewModelFactory (private val correctScore: Int, private val sumScore: Int) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TwoViewModel::class.java)) {
            Log.i("ScoreViewModel", "fac Final correctScore is $correctScore and Final sumScore is $sumScore")
            return TwoViewModel(correctScore,sumScore) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}